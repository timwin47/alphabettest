import {WrapperBlock, Header, DubleContainer, AppBlock} from './Services/GlobalStyled'
import {AlphabetList, MyResultList} from './Views'

function App() {

  return (
    <AppBlock>
      <WrapperBlock>
        <Header>Дюсебаев Темир Бахытжанович</Header>
        <DubleContainer>
          <div>
            <h1>От А до Я</h1>
            <h3>Нажимайте на буквы настолько быстро, насколько вы сможите! Таймер будет запущен автоматически.</h3>
            <AlphabetList />
          </div>
          <MyResultList />
        </DubleContainer>
      </WrapperBlock>
    </AppBlock>
  );
}

export default App;
