

import {WordsList, Timer, TimerCount, Container, WordButton} from './styled'
import {useState, useEffect} from 'react'
import {getNewRandArrayWords, nextWords} from '../../Services/Utils/generateWords'
import Counter from '../../Services/Utils/timer';
import {ReloadButton, ResultTest} from './components'

const AlphabetList = ()=>{
  const [nextListWords, setNextListWords] = useState([])
  const [nextWord, setNextWord] = useState('1')
  const [bgError, setBgError] = useState(null)
  const [activeWords, setActiveWords] = useState([])
  const [timerStatus, setTimerStatus] = useState(null)
  const [resultData, setResultData] = useState({errorsCount: 0, time: '0'})
  const [testStatus, setTestStatus] = useState(false)

  useEffect(()=>{
    updateWords()
  }, [])
  
  const updateWords = ()=>{
    setActiveWords(getNewRandArrayWords())
    let nextListWords =  nextWords()
    nextListWords.reset()
    setNextListWords(nextListWords)
    setNextWord('1')
    setTimerStatus(null)
    setResultData({errorsCount: 0, time: '0'})
    setTestStatus(false)
  }

  const updateWordBlock = (id, next)=>{
    setActiveWords((preActiveWords)=>{
      return preActiveWords.map((el)=>{
        if(el.id === id) return {id: next?.id || id, word: next?.word || ''}
        return el
      })
    })
  }

  const nextWordHundle = (word, id)=>{

    if(timerStatus === null) setTimerStatus(true)

    if(id === nextWord){
      if(word === 'Я'){ //Зовершение теста
        const time = localStorage.getItem('time');
    
        setResultData((preResuldData)=>{
          return {time, errorsCount: preResuldData.errorsCount}
        })
        setTimerStatus(null)  
        updateWordBlock(id)
        return setTestStatus(true)
      }

      setNextWord((pre)=> `${+pre+1}`)
      let newWord = nextListWords.get()
      //слудующая по порядку
      return updateWordBlock(id, newWord)
    }else{
      //Ошибка нажатия порядка
      setResultData((preResult)=>{
        preResult.errorsCount += 1
        return preResult 
      })
     
      setBgError(id)
      setTimeout(()=>{ 
        setBgError(null)
      }, 500)
    }
  }

  return(
    <Container>
      <ReloadButton updateWords={updateWords} />
      {
        testStatus ?
          <ResultTest update={updateWords} resultTestData={resultData} />
        :
          <>
            <Timer><h3>Таймер: </h3><TimerCount>{timerStatus ? <Counter /> : 0}</TimerCount></Timer>
            <WordsList>
              {
                activeWords.map(({word, id})=><WordButton 
                      bgWord={bgError===id}
                      onClick={()=> nextWordHundle(word, id)} 
                      key={id}
                    >
                      {word}
                  </WordButton>
                )
              }
            </WordsList>
          </>
      }
    </Container>
  )
}

export default AlphabetList