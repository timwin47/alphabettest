import {useState} from 'react'
import {ResulBlock, ButtonDefault, InputName} from '../styled'
import { useMutation } from '@apollo/client';
import {
  saveResult
} from '../../../Services/Gqls'
import {actionCreateNewResult} from '../actions'
import {saveTestResult} from '../../../Services/Store'

const ResultTest= ({resultTestData, update})=>{
  const [userName, setUserName] = useState('')

  const [saveNewResult] = useMutation(saveResult, actionCreateNewResult());

  const addResult = ()=>{
     
    let result = { 
      name: userName,
      time_count: +resultTestData.time,
      error_count: resultTestData.errorsCount 
    }

    saveTestResult(result)
    saveNewResult({
      variables: {
        result
      }
    })
    update()
  }

  return(
    <ResulBlock>
      <h3>Тест пройден</h3>

      <label>Имя</label>
      <InputName placeholder='Ваше имя' onChange={(text)=> setUserName(text.target.value)} />
      <p>Ваше время: {resultTestData?.time}</p>
      <p>Количество ошибок: {resultTestData?.errorsCount}</p>

      <ButtonDefault onClick={addResult}>cохранить</ButtonDefault>
    </ResulBlock>
  )
}

export default ResultTest