import {useState} from 'react'
import { 
  mdiReload 
} from '@mdi/js';
import Icon from '@mdi/react'
import {ButtonDefault} from '../styled'

const ReloadButton= ({updateWords})=>{
  const [iconClass, setIconClass] = useState('')

  const update = ()=>{
    setIconClass('update-spiner')
    setTimeout(()=>{
      setIconClass('')
    }, 350)
    updateWords()
  }

  return(
    <ButtonDefault onClick={update}>обновить  <Icon className={iconClass} size={0.7}  path={mdiReload} /></ButtonDefault>
  )
}

export default ReloadButton