import {getTestResults} from '../../../Services/Gqls'
import {sortingResult} from '../../../Services/Utils/sort'
const actionCreateNewResult = ()=>{
  return {
    update: (cache, {data}) => {
      const {saveResult} = data
     
      if(saveResult && saveResult.error === 0){

        const {getResults} = cache.readQuery({
          query: getTestResults,
        })
        let sortRelust = sortingResult([saveResult.result, ...getResults.list])

        cache.writeQuery({
          query: getTestResults,
          data: {
            getResults: {error: 0, list: sortRelust}
          }
        })
        return 'ok'
      }
    },
    onError: (err)=>{
      console.log('SET_RESULT', err)
    }
  }
}

export {actionCreateNewResult}