import styled from 'styled-components'

const WordsList = styled.div`
  display: grid;
  grid-template-columns: 1fr 1fr 1fr 1fr;
  justify-content: center;
  gap: 10px;
  @media (max-width: 760px) {

  }
`
const ButtonDefault = styled.button`
  display: flex;
  font-size: 1.2rem;
  align-items: center;
  justify-content: center;
  width: 150px;
  padding: 5px;
  border: none;
  border-radius: 5px;
  cursor: pointer;
  transition: .5s all;
  margin: 0 auto;
  
  :hover{
    opacity: .6;
  }

  .update-spiner {
    animation:  Icon-spin 1 .3s linear;
  }

  @keyframes Icon-spin {
    from {
      transform: rotate(0deg);
    }
    to {
      transform: rotate(360deg);
    }
  }
`
const Container = styled.div`
  display: flex;
  align-items: center;
  flex-direction: column;
` 

const WordButton = styled.div`
  width: 50px;
  height: 50px;
  display: flex;
  cursor: pointer;
  justify-content: center;
  align-items: center;
  border-radius: 8px;
  color: #fff;
  background: ${({bgWord})=>bgWord ? '#b91919' : '#4291e3'};
  transition: .5s;

  :hover{
    opacity: .8;

  }
`

const Timer = styled.div`
  display: flex;
  align-items: center;
  gap: 15px;
  padding: 15px 0;

  h3{
    margin: 0;
  }
`

const TimerCount = styled.div`
  min-width: 150px;
`

const ResulBlock = styled.div`
  border: 1px solid #f1f1f1;
  border-radius: 10px;
  padding: 20px;
  margin-top: 20px;
  box-sizing: border-box;
  
`

const InputName = styled.input`
  border-radius: 5px;
  border: 1px solid #f1f1f1;
  width: 100%;
  padding: 5px;
  box-sizing: border-box;
`

export {
  Timer,
  InputName,
  ResulBlock,
  TimerCount,
  WordButton,
  ButtonDefault,
  Container,
  WordsList
}