
import {putResults} from '../../../Services/Store'

const actionGetResultTest = (setRatingList)=>{
  return {
    onCompleted: ({getResults})=>{
      if(getResults && getResults.error === 0){
        putResults(getResults.list)
        return setRatingList(getResults.list)
      }
    },
    onError: (err)=>{
      console.log('getResults', err)
    }
  }
}

export {actionGetResultTest}