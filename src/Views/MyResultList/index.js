import {Container} from './styled'
import {useState, useEffect} from 'react'
import {getMyTestRelust} from '../../Services/Store'
import {ItemResult, HeaderResultList} from '../../GlobalComponents'
import {ResultList} from '../../Services/GlobalStyled'
import {getTestResults} from '../../Services/Gqls'
import {actionGetResultTest} from './actions'
import InfiniteScroll from 'react-infinite-scroll-component';

import { 
  useQuery,
} from '@apollo/client';

const MyResultList = ()=>{
  const [ratingList, setRatingList] = useState([])

  useQuery(getTestResults, actionGetResultTest(setRatingList));

  useEffect(()=>{
    setRatingList(getMyTestRelust())
  }, [])

  return(
    <Container>
      <h1>Результаты тестов</h1>
      <ResultList>
        <HeaderResultList />
        <InfiniteScroll
          dataLength={ratingList.length}
          scrollableTarget="scrollableDiv"
        >
          {ratingList.map((userResult, index) => <ItemResult key={userResult._id} index={index+1} userResult={userResult} />)}   
        </InfiniteScroll>
      </ResultList>
    </Container>
  )
}

export default MyResultList