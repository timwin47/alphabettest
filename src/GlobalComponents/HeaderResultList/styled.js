
import styled from 'styled-components'

const HeadBlock = styled.div`
  display: grid;
  text-align: center;
  gap: 10px;
  border-bottom: 1px solid #f1f1f1;
  padding: 10px 5px;
  grid-template-columns: 1fr 1fr 1fr 1fr;
` 

export {
  HeadBlock
}