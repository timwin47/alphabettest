
import styled from 'styled-components'


const ItemBlock = styled.div`
  display: grid;
  width: 100%;
  padding: 10px 5px;
  border-bottom: 1px dotted #f1f1f1;
  text-align: center;
  gap: 10px;
  box-sizing: border-box;
  justyfy-content: flex-start;
  grid-template-columns: 1fr 1fr 1fr 1fr;
  transition: .5s all;
  :hover{
    background: #f1f1f1;
  }
` 

export {
  ItemBlock
}