import {ItemBlock} from './styled'
import {useState} from 'react'

// import { 
//   mdiReload 
// } from '@mdi/js';
// import Icon from '@mdi/react'

 const ItemResult = ({userResult, index})=>{
  const [ratingList, setRatingList] = useState([])

  return(
    <ItemBlock>
      <span>{index}</span>
      <span>{userResult.name}</span>
      <span>{userResult.time_count}</span>
      <span>{userResult.error_count}</span>
    </ItemBlock>
  )
}

export default ItemResult