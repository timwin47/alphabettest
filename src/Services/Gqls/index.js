import { gql } from '@apollo/client';

const getTestResults = gql`
  query {
    getResults{
      error
      list{
        _id
        name
        error_count
        time_count
      }
    }
  }
`;

const saveResult = gql`
  mutation ($result: StatsData!){
    saveResult(result: $result) {
      error
      result{
        _id
        name
        error_count
        time_count
      }
    }
  }
`;

export {getTestResults, saveResult}