
import { 
  ApolloClient,
  InMemoryCache,
  from,
} from '@apollo/client';

import { createUploadLink } from 'apollo-upload-client';
import config from '../../Config';

const apolloClientSetup = () => {

	const link = from([
		createUploadLink({ uri: config.graphqlURI }),
	]);

	const client = new ApolloClient({
	  link,
	  cache: new InMemoryCache()
	});

  return client;
}

export default apolloClientSetup;

