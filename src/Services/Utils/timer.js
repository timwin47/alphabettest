import {useState, useEffect} from 'react'

const Counter = ()=>{

  const [count, setCount] = useState(0);

  useEffect(() => {
    const timer = setTimeout(() => {
      setCount(pre=>pre+1);
    }, 1000);
    return () => {
      localStorage.setItem('time', count)
      clearTimeout(timer)
    };
  }, [count]);

  return count
}

export default Counter