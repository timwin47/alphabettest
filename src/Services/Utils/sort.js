const sortingResult = (sortRelust)=>{
  return sortRelust.sort(function (a, b) {
    if (a.error_count > b.error_count) {
      return 1;
    }
    if (a.error_count < b.error_count) {
      return -1;
    }
    return 0;
  }).sort(function (a, b) {
    if (+a.time_count > +b.time_count) {
      return 1;
    }
    if (+a.time_count < +b.time_count) {
      return -1;
    }
    return 0;
  })
}
export {sortingResult}