
import {
  alphabetOne, 
  alphabetTwo
} from './constans'

const getNewRandArrayWords = ()=>{
  let alphaClone = structuredClone(alphabetOne);

  let randomWords = []

  alphabetOne.forEach(()=>{
    let random = Math.floor(Math.random()*alphaClone.length);
    randomWords.push(alphaClone[random])
    alphaClone.splice(random, 1)
  })

  return randomWords
}

const nextWords = ()=>{
  let listAlphabetTwo = structuredClone(alphabetTwo);

  return {
    get() {return listAlphabetTwo.shift()},
    reset() { listAlphabetTwo = structuredClone(alphabetTwo); }
  };

}

export {getNewRandArrayWords, nextWords}