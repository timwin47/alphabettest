
const colors = {
  red: '#b91919',
  blue: '#4291e3'
}

const alphabetOne = [
  {id: '1', word: "А"},
  {id: '2', word: "Б"},
  {id: '3', word: "В"},
  {id: '4', word: "Г"},
  {id: '5', word: "Д"},
  {id: '6', word: "Е"},
  {id: '7', word: "Ё"},
  {id: '8', word: "Ж"},
  {id: '9', word: "З"},
  {id: '10', word: "И"},
  {id: '11', word: "Й"},
  {id: '12', word: "К"},
  {id: '13', word: "Л"},
  {id: '14', word: "М"},
  {id: '15', word: "Н"},
  {id: '16', word: "О"},
]
const alphabetTwo = [
  {id: '17', word: "П"},
  {id: '18', word: "Р"},
  {id: '19', word: "С"},
  {id: '20', word: "Т"},
  {id: '21', word: "У"},
  {id: '22', word: "Ф"},
  {id: '23', word: "Х"},
  {id: '24', word: "Ц"},
  {id: '25', word: "Ч"},
  {id: '26', word: "Ш"},
  {id: '27', word: "Щ"},
  {id: '28', word: "Ъ"},
  {id: '29', word: "Ы"},
  {id: '30', word: "Ь"},
  {id: '31', word: "Э"},
  {id: '32', word: "Ю"},
  {id: '33', word: "Я"},
]

export {
  colors,
  alphabetOne,
  alphabetTwo
}