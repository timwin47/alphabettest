import styled from 'styled-components'

const WrapperBlock = styled.div`
  display: flex;
  align-items: center;
  flex-direction: column;
  width: 1000px;

  h1{
    text-align: center;
  }

  h3{
    font-weight: 100;
    text-align: center;
  }
  
	@media (max-width: 960px) {

	}
`

const AppBlock = styled.div`
  display: flex;
  justify-content: center;
`

const Header = styled.h2`
  padding: 15px;
  text-align: center;
  color: #fff;
  margin: 0;
  font-weight: 100;
  width: 100%;
  background: #4291e3;
`
const ResultList = styled.div`
  display: grid;
  grid: 15px;
`
const DubleContainer = styled.div`
  display: grid;
  padding: 15px;
  grid-template-columns: 1fr 1fr;
  width: 100%;

  @media (max-width: 960px) {
    grid-template-columns: 1fr;
  }
`

export {
  AppBlock,
  Header,
  ResultList,
  DubleContainer,
  WrapperBlock,
}