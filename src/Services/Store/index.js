import {sortingResult} from '../Utils/sort'

const saveTestResult = (result)=>{
  const myResults = localStorage.getItem('myResults')
  let localResult = {...result, _id: Math.floor(Math.random() * 1001)}
  
  if(myResults){
    let myResultsPars = JSON.parse(myResults)
    myResultsPars.push(localResult)
    localStorage.setItem('myResults', JSON.stringify(myResultsPars))
    return 
  }
    
  localStorage.setItem('myResults', JSON.stringify([localResult])) 
}

const putResults = (results)=>{
  localStorage.setItem('myResults', JSON.stringify(results))
}

const getMyTestRelust = ()=>{
  const myResults = localStorage.getItem('myResults')

  return myResults ? sortingResult(JSON.parse(myResults)) : []
}

export {saveTestResult, putResults, getMyTestRelust}